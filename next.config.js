const path = require("path");

/** @type {import('next').NextConfig} */
const nextConfig = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, nextRuntime, webpack }) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      redux: path.resolve(__dirname, "node_modules/redux"),
    };
    // Important: return the modified config
    return config;
  },
};

module.exports = nextConfig;
