import Container from "@ui/Container";

import styles from "./Footer.module.scss";

const FooterView: React.FC = () => {
  return (
    <footer className={styles.footer}>
      <Container className={styles.container}>
        <div className={styles.links}></div>
        <div className={styles.info}></div>
      </Container>
    </footer>
  );
};

export default FooterView;
