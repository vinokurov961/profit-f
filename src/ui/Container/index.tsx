import styles from "./Container.module.scss";

type TContainerProps = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

const Container: React.FC<React.PropsWithChildren<TContainerProps>> = props => {
  return (
    <div
      {...props}
      className={["container", styles.container, props.className].join(" ")}
    >
      {props.children}
    </div>
  );
};

export default Container;
